__author__ = 'ReedApps'

import RC_Import_Models as Model
import RC_Import_File_Ind as Ind
import RC_Import_Data_Access as DataAccess
import RC_Import_Support_Methods as Support
import RC_Import_Magic_Strings as MagicStrings
from decimal import *
import csv
import os
import sys

from progressbar import Timer, ProgressBar, AnimatedMarker

def ExtractAndBuildModel(fileName):
    print 'Extraction Began'

    clientStarted = False
    totalStarted = False
    isSalesPersonEnd = False
    salesPersonList = []
    currentSalesPerson = Model.SalesPerson()
    currentClient = Model.Client()

    try:
        with open(fileName) as RCfile:
            for lineFeed in RCfile:

                lineFeed = Support.cleanLineFeed(lineFeed)
                isSalesPerson = Ind.isSalesPersonLine(lineFeed)
                isClient = Ind.isNewClientLine(lineFeed)
                isInvoice = Ind.isInvoiceLine(lineFeed)
                isClientTotal = Ind.isClientTotalLine(lineFeed)
                isSalesPersonTotal = Ind.isSalesPersonTotalLine(lineFeed)
                isRodoTotal = Ind.isRodoTotalLine(lineFeed)
                isAeroTotal = Ind.isAeroTotalLine(lineFeed)
                isLogisticaTotal = Ind.isLogisticaTotalLine(lineFeed)
                charCount = len(lineFeed)

                if(isSalesPerson):
                    currentSalesPerson = Support.buildSalesPerson(lineFeed, currentSalesPerson)
                elif(isClient and clientStarted == False and charCount <= 140):
                    currentClient = Support.buildClient(lineFeed, currentClient)
                    clientStarted = True
                elif(isInvoice):
                    currentClient.InvoiceList.append(Support.buildInvoice(lineFeed))
                elif(isClientTotal):
                    currentClient = Support.appendClientTotals(lineFeed, currentClient)
                    currentSalesPerson.ClientList.append(currentClient)
                    currentClient = Model.Client()
                    clientStarted = False
                elif(isSalesPersonTotal and totalStarted == False):
                    currentSalesPerson = Support.appendSalesPersonTotals(lineFeed, currentSalesPerson)
                    totalStarted = True
                elif(isRodoTotal):
                    currentSalesPerson.setFinalRODOLiquidValueTotal(Support.getRodoTotal(lineFeed))
                elif(isAeroTotal):
                    currentSalesPerson.setFinalAEROLiquidValueTotal(Support.getAeroTotal(lineFeed))
                elif(isLogisticaTotal):
                    currentSalesPerson.setFinalLogisticalLiquidValueTotal(Support.getLogisticaTotal(lineFeed))
                    isSalesPersonEnd = True

                if(isSalesPersonEnd):
                    salesPersonList.append(currentSalesPerson)

                    #Reinitialize flags and objects
                    isSalesPersonEnd = False
                    totalStarted = False
                    currentSalesPerson = Model.SalesPerson()

    except IOError:
        print'An error occurred in ExtractAndBuildModel() method'

    return salesPersonList

def RunSQLscripts(totalScriptCount):
    datafeed = []
    percentmarker = 0.02
    runScriptCount = 0
    fileName = os.path.join('SupportDocs', 'SqlScripts.txt')
    try:
        conn = DataAccess.OpenTeradataConnection()
        #widgets = ['Working: ', AnimatedMarker(),' (', Timer(), ')']
        #pbar = ProgressBar(widgets=widgets, maxval=totalScriptCount).start()
        #for i in pbar((i for i in range(totalScriptCount))):
        with open(fileName) as ScriptFile:
            for i, script in enumerate(ScriptFile):
                scriptArray = script.split(':')
                select = scriptArray[1]
                cursor = conn.cursor()
                cursor.execute(select)
                results = cursor.fetchall()
                results = Support.correctErrorsForResults(results, scriptArray)
                datafeed.append(results)
                runScriptCount +=1
                percent = Decimal(runScriptCount) / Decimal(totalScriptCount)
                if(percent >= percentmarker):
                    print "%d%% of scripts run" % (percentmarker * 100)
                    percentmarker += 0.02
                #pbar.update(i+1)
        WriteEDWResultsToCSV(datafeed)
        #pbar.finish()
        DataAccess.CloseConnection(conn)
    except IOError:
        print'An error occurred in the RunSqlScripts method'
    print 'Running Scripts'

def WriteEDWResultsToCSV(results):
    print 'Importing EDW results to CSV'
    try:
        filename = raw_input('EDW results file name: ')
        csvfile = open(filename + '.csv', 'wb')
        writer = csv.writer(csvfile)
        for recordArray in results:
            for records in recordArray:
                for record in records:
                    writer.writerow(record)
    except IOError:
        print'An error occured in the WriteEDWResultsToCSV() Method'

def ImportRCFileToCSV(salesPeople):
    print 'Importing to CSV'
    try:
        filename = raw_input('File name of RC Import: ')
        csvfile = open(filename + '.csv', 'wb')
        writer = csv.writer(csvfile)
        writer.writerow(Support.buildRCHeaders())
        RowArray = []
        for salesPerson in salesPeople:
            salesPersonArray = Support.createCSVRow(salesPerson)
            RowArray.append(salesPersonArray)
        for rows in RowArray:
            for row in rows:
                writer.writerow(row)
    except IOError:
        print'An error occurred in the ImportRCFileToCSV() Method'

#TODO:Finish this method
def ImportRCFileToMySql(salesPeople):
    print 'Importing to MySql'

    conn = DataAccess.OpenMySqlConnection()
    salesPersonID = 0
    clientID = 0
    crsr = conn.cursor()
    for salesPerson in salesPeople:
        salesPersonID = salesPerson.salesPersonID
        insertSalesPersonArray = Support.generateInsertSalesPersonArray(salesPerson)
        conn.execute(MagicStrings.add_Sales_Person_MySql, insertSalesPersonArray)
        conn.commit()
        for client in salesPerson.getClientList():
            clientID = client.ClientID
            insertClientArray = Support.generateInsertClientArray(client,salesPersonID)
            conn.execute(MagicStrings.add_Client_MySql, insertClientArray)
            conn.commit()
            for invoice in client.getInvoiceList():
                insertInvoiceArray = Support.generateInsertInvoiceArray(invoice)
                conn.execute(MagicStrings.add_Invoice_MySql, insertInvoiceArray)
                conn.commit()
    DataAccess.CloseConnection(conn)

def CreateSQLScripts(salesPeople):
    
    scriptCount = 0
    try:
        fo = open('SupportDocs/SqlScripts.txt','w')

        for salesPerson in salesPeople:
            spScriptArray = Support.buildSQLscripts(salesPerson)
            for script in spScriptArray:
                fo.write(script + '\n')
                scriptCount += 1
                
    except IOError:
        print 'An error occured in the I/O of SqlScripts.txt'
    spScriptArray
    return scriptCount

def main():
    print'Author: ' + __author__
    print'Python Script for Extracting Data from RC file.'
    print'***Warning: Manipulation of any files created/worked on \n' \
         'by this script during run will result in failure.'
    fileName = raw_input('Enter the filename(including the extension): ')
    print'***************************************************************'

    fullPath = os.path.join('SupportDocs', fileName)

    salesPeople = ExtractAndBuildModel(fullPath)
    print'Captured:'
    print'\t' + str(Model.SalesPerson.salesPersonID) + ' Sales People.'
    print'\t' + str(Model.Client.ClientID) + ' Clients.'
    print'\t' + str(Model.Invoice.InvoiceID) + ' Invoices.'

    ImportRCFileToCSV(salesPeople)

    createScripts = raw_input('Would you like to create the SQL scripts? (Y/N): ')
    if(createScripts.upper() == 'Y'):
        scriptCount = CreateSQLScripts(salesPeople)
        print str(scriptCount) + ' scripts created.'
        print('Would you like to run the SQL scripts?')
        print('***Warning: This will take several minutes to complete')
        runScripts = raw_input('(Y/N): ')
        if(runScripts.upper() == 'Y'):
            RunSQLscripts(scriptCount)
        else:
            print 'Thank you. Process Terminated'
            sys.exit(0)
    else:
        print 'Thank you. Process Terminated'
        sys.exit(0)

main()
