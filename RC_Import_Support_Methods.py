__author__ = 'ReedApps'

import RC_Import_Models as Model
import RC_Import_Magic_Strings as MagicStrings
from datetime import date, datetime
from decimal import *

chars_to_remove = ['.','/','-','%']
now = datetime.now().date()

def buildSalesPerson(lineFeed, salesPerson):

    lineArray = lineFeed.split()

    salesPerson.setSalesPersonNumber(lineArray[1])
    salesPerson.setFirstName(lineArray[3])
    salesPerson.setLastName(lineArray[4])

    return salesPerson

def buildClient(lineFeed, client):

    lineArray = lineFeed.split()
    commissionPosition = len(lineArray)- 1
    bonusPosition = len(lineArray) - 2
    serviceProvidedPosition = len(lineArray) - 3

    client.setCPNJraw(lineArray[2])
    client.setCPNJstripped(lineArray[2]
                           .translate(None,''.join(chars_to_remove)))
    client.setName(getClientName(lineFeed))
    client.setServiceProvided(lineArray[serviceProvidedPosition])
    client.setBonusPercentage((lineArray[bonusPosition]
                               .translate(None,''.join(chars_to_remove))).replace(',', '.'))
    client.setCommissionPercentage((lineArray[commissionPosition]
                                    .translate(None,''.join(chars_to_remove))).replace(',', '.'))

    return client

def buildInvoice(lineFeed):

    invoice = Model.Invoice()
    lineArray = lineFeed.split()

    invoice.setInvoiceNumber(str(lineArray[1].translate(None,''.join(chars_to_remove))))
    invoice.setLiquidDate(lineArray[2])
    invoice.setInvoiceValue(lineArray[3].replace(',', '.'))
    invoice.setTax(lineArray[4].replace(',', '.'))
    invoice.setDiscount(lineArray[5].replace(',', '.'))
    invoice.setLiquidValue(lineArray[6].replace(',', '.'))
    invoice.setPayDate(lineArray[7])
    invoice.setServicePayDate(lineArray[8])
    invoice.setDifference(lineArray[9])
    invoice.setFinalValue(lineArray[10].replace(',', '.'))
    invoice.setPercentage(lineArray[11].replace('%', ''))
    invoice.setCommissionValue(lineArray[12].replace(',', '.'))

    return invoice

def appendClientTotals(lineFeed, client):

    lineArray = lineFeed.split()

    client.setInvoiceFinalValueTotal(lineArray[4].replace(',', '.'))
    client.setInvoiceTaxTotal(lineArray[5].replace(',', '.'))
    client.setInvoiceDiscountTotal(lineArray[6].replace(',', '.'))
    client.setInvoiceLiquidTotal(lineArray[7].replace(',', '.'))
    client.setInvoiceFinalValueTotal(getInvoiceFinalTotal(client.getInvoiceList()))

    if(len(lineArray[8]) >= 12):
        client.setInvoiceBonusTotal(lineArray[8][0:4].replace(',','.'))
        client.setInvoiceCommissionTotal(lineArray[8][4:len(lineArray[8])].replace(',','.'))
    else:
        client.setInvoiceBonusTotal(lineArray[8].replace(',', '.'))
        client.setInvoiceCommissionTotal(lineArray[9].replace(',', '.'))

    return client

def appendSalesPersonTotals(lineFeed, salesPerson):

    lineArray = lineFeed.split()
    salesPerson.setFinalInvoiceTotal(lineArray[3].replace('.','').replace(',', '.'))
    salesPerson.setFinalInvoiceTaxTotal(lineArray[4].replace('.','').replace(',', '.'))
    salesPerson.setFinalInvoiceDiscountTotal(lineArray[5].replace('.','').replace(',', '.'))
    salesPerson.setFinalInvoiceLiquidTotal(lineArray[6].replace('.','').replace(',', '.'))
    salesPerson.setFinalInvoiceFinalValueTotal(lineArray[7].replace('.','').replace(',', '.'))
    salesPerson.setFinalInvoiceCommissionTotal(lineArray[8].replace('.','').replace(',', '.'))
    return salesPerson

def getRodoTotal(lineFeed):

    lineArray = lineFeed.split()
    total = lineArray[5].replace('.','').replace(',', '.')
    return Decimal(total)

def getAeroTotal(lineFeed):

    lineArray = lineFeed.split()
    total = lineArray[5].replace('.','').replace(',', '.')
    return Decimal(total)

def getLogisticaTotal(lineFeed):

    lineArray = lineFeed.split()
    total = lineArray[5].replace('.','').replace(',', '.')
    return Decimal(total)

def buildSQLscripts(salesPerson):

    scripts = []
    SQLselectWhere = MagicStrings.SQLselectWhere
    SQLand = MagicStrings.SQLand
    SQLend = MagicStrings.SQLend
    invCount = 0

    try:
        for client in salesPerson.getClientList():
            sqlScript = SQLselectWhere + client.getCPNJstripped() + SQLand
            for invoice in client.getInvoiceList():
                if(invCount == 0):
                    sqlScript += invoice.getInvoiceNumber()
                else:
                    sqlScript +="," + invoice.getInvoiceNumber()
                invCount += 1
            sqlScript = client.getServiceProvided() + ':' + sqlScript + " )" + SQLend
            scripts.append(sqlScript)
            invCount = 0
    except TypeError:
        print "TypeError occured in buildSQLscripts method in Support Methods"

    return scripts

def getClientName(lineFeed):

    lineArray = lineFeed.split()

    startPosition = lineFeed.find(lineArray[3])
    endPosition = 60

    clientName = lineFeed[startPosition:endPosition].strip()
    return clientName

def getInvoiceFinalTotal(InvoiceList):
    total = 0.00
    for invoice in InvoiceList:
        total += float(invoice.getFinalValue())

    return total

def buildRCHeaders():

    headerArray = ['SalesPersonID', 'SalesPersonName', 'CPNJraw', 'CPNJstripped', 'ClientName', 'ServiceProvided',
                   'Bonus%', 'Commission%', 'InvoiceNumber', 'LiqDate', 'InvoiceAmount', 'Tax', 'Discount',
                   'LiquidAmount', 'PayDate', 'ServicePayDate', 'Difference', 'FinalAmount', 'Percentage',
                   'BonusAmount', 'CommissionAmount']

    return headerArray

def createCSVRow(salesPerson):

    rowArray = []

    try:
        for client in salesPerson.getClientList():
            for invoice in client.getInvoiceList():
                invoiceArray = [salesPerson.getSalesPersonNumber(), salesPerson.getFullName(), client.getCPNJraw(),
                            client.getCPNJstripped(), client.getName(), client.getServiceProvided(),
                            client.getBonusPercentage(), client.getCommissionPercentage(), invoice.getInvoiceNumber(),
                            invoice.getLiquidDate(), invoice.getInvoiceValue(), invoice.getTax(), invoice.getDiscount(),
                            invoice.getLiquidValue(), invoice.getPayDate(), invoice.getServicePayDate(),
                            invoice.getDifference(), invoice.getFinalValue(), invoice.getPercentage(),
                            invoice.getBonusInvoiceValue(), invoice.getCommissionValue()]
                rowArray.append(invoiceArray)
    except IndexError:
        print "Error in the createCSVRow() Method in the Support Methods Module"

    return rowArray

def correctErrorsForResults(answers, scriptArray):
    #TODO: finish out this method
    acctType = scriptArray[0]
    select = scriptArray[1]
    results=[]
    manAnswers = []
    brokenSelectArray = select.split()
    Invoices = brokenSelectArray[29]
    CPNJ = brokenSelectArray[24]
    testList = []
    masterList = []
    if(answers == []):
        for inv in Invoices.split(','):
            manAnswers.append((CPNJ,acctType,inv,'N/A','N/A','N/A'))
    else:
        i = 0
        InvoiceArray = Invoices.split(',')
        #prep list
        for inv in InvoiceArray:
            testList.append((CPNJ,acctType,inv,'N/A','N/A','N/A'))
        for mstrInv in testList:
            mstlst = list(mstrInv)
            for ans in answers:
                ans = list(ans)
                if(ans[1] == mstlst[2]):
                    ans.insert(1,acctType)
                    mstlst = ans
                    break
            masterList.append(mstlst)
    results.append(masterList)
    return results

def generateInsertSalesPersonArray(salesPerson):

    salesPersonArray = {
        'firstName': salesPerson.getFirstName(),
        'lastName': salesPerson.getLastName(),
        'salesNumber': salesPerson.getSalesPersonNumber(),
        'finalInvoiceTotal': salesPerson.getFinalInvoiceDiscountTotal(),
        'finalInvoiceDiscountTotal': salesPerson.getFinalInvoiceDiscountTotal(),
        'finalInvoiceLiquidTotal': salesPerson.getFinalInvoiceLiquidTotal(),
        'finalInvoiceFinalValueTotal': salesPerson.getFinalInvoiceFinalValueTotal(),
        'finalInvoiceCommissionTotal': salesPerson.getFinalInvoiceCommissionTotal(),
        'finalRODOLiquidValueTotal': salesPerson.getFinalRODOLiquidValueTotal(),
        'finalAEROLiquidValueTotal': salesPerson.getFinalAEROLiquidValueTotal(),
        'finalLogisticalLiquidValueTotal': salesPerson.getFinalLogisticalLiquidValueTotal(),
        'dateCreated': now,
        'dateModified': now,
        'userCreated': 'admin',
        'userModified': 'admin'
    }
    return salesPersonArray

def generateInsertClientArray(client, salesPersonID):
    print''
    clientArray = {
        'salesPersonID':salesPersonID,
        'cpnjStripped':client.getCPNJstripped()
    }

def generateInsertInvoiceArray(invoice, salesPersonID, clientID):
    print''

def cleanLineFeed(lineFeed):
    lineFeed = lineFeed.replace('\x1b','')
    lineFeed = lineFeed.replace('\x00',' ')
    return lineFeed