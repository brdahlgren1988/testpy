__author__ = 'ReedApps'

class SalesPerson:
    salesPersonID = 0

    def getFirstName(self):
        return self.FirstName

    def getLastName(self):
        return self.LastName

    def getFullName(self):
        return self.FirstName + " " + self.LastName

    def getSalesPersonNumber(self):
        return self.SalesNumber

    def getFinalInvoiceTotal(self):
        return self.FinalInvoiceTotal

    def getFinalInvoiceTaxTotal(self):
        return self.FinalInvoiceTaxTotal

    def getFinalInvoiceDiscountTotal(self):
        return self.FinalInvoiceDiscountTotal

    def getFinalInvoiceLiquidTotal(self):
        return self.FinalInvoiceLiquidTotal

    def getFinalInvoiceFinalValueTotal(self):
        return self.FinalInvoiceFinalValueTotal

    def getFinalInvoiceCommissionTotal(self):
        return self.FinalCommissionTotal

    def getFinalRODOLiquidValueTotal(self):
        return self.FinalRODOLiquidValueTotal

    def getFinalAEROLiquidValueTotal(self):
        return self.FinalAEROLiquidValueTotal

    def getFinalLogisticalLiquidValueTotal(self):
        return self.FinalLogisticalLiquidValueTotal

    def getClientList(self):
        return self.ClientList

    def setFirstName(self, firstName):
        self.FirstName = firstName

    def setLastName(self, lastName):
        self.LastName = lastName

    def setSalesPersonNumber(self, salesNumber):
        self.SalesNumber = salesNumber

    def setFinalInvoiceTotal(self, finalInvoiceTotal):
        self.FinalInvoiceTotal = finalInvoiceTotal

    def setFinalInvoiceTaxTotal(self, finalInvoiceTaxTotal):
        self.FinalInvoiceTaxTotal = finalInvoiceTaxTotal

    def setFinalInvoiceDiscountTotal(self, finalInvoiceDiscountTotal):
        self.FinalInvoiceDiscountTotal = finalInvoiceDiscountTotal

    def setFinalInvoiceLiquidTotal(self, finalinvoiceLiquidTotal):
        self.FinalInvoiceLiquidTotal = finalinvoiceLiquidTotal

    def setFinalInvoiceFinalValueTotal(self, finalInvoiceFinalValueTotal):
        self.FinalInvoiceFinalValueTotal = finalInvoiceFinalValueTotal

    def setFinalInvoiceCommissionTotal(self, finalCommissionTotal):
        self.FinalCommissionTotal = finalCommissionTotal

    def setFinalRODOLiquidValueTotal(self, finalRODOLiquidValueTotal):
        self.FinalRODOLiquidValueTotal = finalRODOLiquidValueTotal

    def setFinalAEROLiquidValueTotal(self, finalAEROLiquiValueTotal):
        self.FinalAEROLiquidValueTotal = finalAEROLiquiValueTotal

    def setFinalLogisticalLiquidValueTotal(self, finalLogiticalLiquidValueTotal):
        self.FinalLogisticalLiquidValueTotal = finalLogiticalLiquidValueTotal

    def setClientList(self, ClientList):
        self.ClientList = ClientList

    def appendClientToList(self, client):
        self.ClientList.append(client)

    def __init__(self):
        self.FirstName = ""
        self.LastName = ""
        self.FullName = ""
        self.SalesPersonNumber = ""
        self.FinalInvoiceTotal = 0.00
        self.FinalInvoiceTaxTotal = 0.00
        self.FinalInvoiceDiscountTotal = 0.00
        self.FinalInvoiceLiquidTotal = 0.00
        self.FinalInvoiceFinalValueTotal = 0.00
        self.FinalCommissionTotal = 0.00
        self.FinalRODOLiquidValueTotal = 0.00
        self.FinalAEROLiquidValueTotal = 0.00
        self.FinalLogisticalLiquidValueTotal = 0.00
        self.ClientList = []
        SalesPerson.salesPersonID += 1

class Client:
    ClientID = 0

    def getCPNJstripped(self):
        return self.CPNJstripped

    def getCPNJraw(self):
        return self.CPNJraw

    def getName(self):
        return self.Name

    def getServiceProvided(self):
        return self.ServiceProvided

    def getBonusPercentage(self):
        return self.BonusPercentage

    def getCommissionPercentage(self):
        return self.CommissionPercentage

    def getInvoiceTotal(self):
        return self.InvoiceTotal

    def getInvoiceTaxTotal(self):
        return self.InvoiceTaxTotal

    def getInvoiceDiscountTotal(self):
        return self.InvoiceDiscountTotal

    def getInvoiceLiquidTotal(self):
        return self.InvoiceLiquidTotal

    def getInvoiceFinalValueTotal(self):
        return self.InvoiceFinalValueTotal

    def getInvoiceBonusTotal(self):
        return self.BonusTotal

    def getInvoiceCommissionTotal(self):
        return self.CommissionTotal

    def getInvoiceList(self):
        return self.InvoiceList

    def setCPNJstripped(self, CPNJstripped):
        self.CPNJstripped = CPNJstripped

    def setCPNJraw(self, CPNJraw):
        self.CPNJraw = CPNJraw

    def setName(self, name):
        self.Name = name

    def setServiceProvided(self, serviceProvided):
        self.ServiceProvided = serviceProvided

    def setBonusPercentage(self, bonusPercentage):
        self.BonusPercentage = bonusPercentage

    def setCommissionPercentage(self, commissionPercentage):
        self.CommissionPercentage = commissionPercentage

    def setInvoiceTotal(self, invoiceTotal):
        self.InvoiceTotal = invoiceTotal

    def setInvoiceTaxTotal(self, invoiceTaxTotal):
        self.InvoiceTaxTotal = invoiceTaxTotal

    def setInvoiceDiscountTotal(self, invoiceDiscountTotal):
        self.InvoiceDiscountTotal = invoiceDiscountTotal

    def setInvoiceLiquidTotal(self, invoiceLiquidTotal):
        self.InvoiceLiquidTotal = invoiceLiquidTotal

    def setInvoiceFinalValueTotal(self, invoiceFinalValueTotal):
        self.InvoiceFinalValueTotal = invoiceFinalValueTotal

    def setInvoiceBonusTotal(self, bonusTotal):
        self.BonusTotal = bonusTotal

    def setInvoiceCommissionTotal(self, commissionTotal):
        self.CommissionTotal = commissionTotal

    def setInvoiceList(self, InvoiceList):
        self.InvoiceList = InvoiceList

    def appendInvoiceToList(self, invoice):
        self.InvoiceList.append(invoice)

    def __init__(self):
        self.CPNJstripped = ""
        self.CPNJraw = ""
        self.Name = ""
        self.ServiceProvided = ""
        self.BonusPercentage = 0.00
        self.CommissionPercentage = 0.00
        self.InvoiceTotal = 0.00
        self.InvoiceTaxTotal = 0.00
        self.InvoiceDiscountTotal = 0.00
        self.InvoiceLiquidTotal = 0.00
        self.InvoiceFinalValueTotal = 0.00
        self.BonusTotal = 0.00
        self.CommissionTotal = 0.00
        self.InvoiceList = []
        Client.ClientID += 1

class Invoice:
    InvoiceID = 0

    def __init__(self):
        self.InvoiceNumber = ""
        self.LiquidDate = ""
        self.InvoiceValue = 0.00
        self.Tax = 0.00
        self.Discount = 0.00
        self.LiquidValue = 0.00
        self.PayDate = ""
        self.ServicePayDate = ""
        self.Difference = ""
        self.FinalValue = 0.00
        self.Percentage = 0.00
        self.BonusInvoiceValue = 0.00
        self.CommissionValue = 0.00
        Invoice.InvoiceID += 1

    def getInvoiceNumber(self):
        return self.InvoiceNumber

    def getLiquidDate(self):
        return self.LiquidDate

    def getInvoiceValue(self):
        return self.InvoiceValue

    def getTax(self):
        return self.Tax

    def getDiscount(self):
        return self.Discount

    def getLiquidValue(self):
        return self.LiquidValue

    def getPayDate(self):
        return self.PayDate

    def getServicePayDate(self):
        return self.ServicePayDate

    def getDifference(self):
        return self.Difference

    def getFinalValue(self):
        return self.FinalValue

    def getPercentage(self):
        return self.Percentage

    def getBonusInvoiceValue(self):
        return self.BonusInvoiceValue

    def getCommissionValue(self):
        return self.CommissionValue

    def setInvoiceNumber(self, invoiceNumber):
        self.InvoiceNumber = invoiceNumber

    def setLiquidDate(self, liquidDate):
        self.LiquidDate = liquidDate

    def setInvoiceValue(self, invoiceValue):
        self.InvoiceValue = invoiceValue

    def setTax(self, tax):
        self.Tax = tax

    def setDiscount(self, discount):
        self.Discount = discount

    def setLiquidValue(self, liquidValue):
        self.LiquidValue = liquidValue

    def setPayDate(self, payDate):
        self.PayDate = payDate

    def setServicePayDate(self, servicePayDate):
        self.ServicePayDate = servicePayDate

    def setDifference(self, difference):
        self.Difference = difference

    def setFinalValue(self, finalValue):
        self.FinalValue = finalValue

    def setPercentage(self, percentage):
        self.Percentage = percentage

    def setBonusInvoiceValue(self, bonusInvoiceValue):
        self.BonusInvoiceValue = bonusInvoiceValue

    def setCommissionValue(self, commissionValue):
        self.CommissionValue = commissionValue
