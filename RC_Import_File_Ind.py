__author__ = 'ReedApps'
import RC_Import_Magic_Strings as MagicStrings

#This file is for the Indicators for reading the RC file
#included here to keep clutter in main file

def isSalesPersonLine(lineFeed):

    SalesPersonMarker = lineFeed.find(MagicStrings.SalesPersonInd)
    if(SalesPersonMarker != -1):
        return True
    else:
        return False

def isNewClientLine(lineFeed):

    ClientMarker = lineFeed.find(MagicStrings.ClientInd)
    if(ClientMarker != -1):
        return True
    else:
        return False

def isInvoiceLine(lineFeed):

    InvMarker2015 = lineFeed.find(MagicStrings.InvoiceInd2015)
    InvMarker2014 = lineFeed.find(MagicStrings.InvoiceInd2014)
    if((InvMarker2015 != -1 and InvMarker2015 <= 17) or (InvMarker2014 != -1 and InvMarker2014 <= 17)):
        return True
    else:
        return False

def isClientTotalLine(lineFeed):

    ClientTotalMarker = lineFeed.find(MagicStrings.ClientTotalInd)
    if(ClientTotalMarker != -1):
        return True
    else:
        return False

def isSalesPersonTotalLine(lineFeed):

    SalesPersonTotalMarker = lineFeed.find(MagicStrings.SalesPersonTotalInd)
    if(SalesPersonTotalMarker != -1):
        return True
    else:
        return False

def isRodoTotalLine(lineFeed):

    RodoMarker = lineFeed.find(MagicStrings.RodoInd)
    if(RodoMarker != -1):
        return True
    else:
        return False

def isAeroTotalLine(lineFeed):

    AeroMarker = lineFeed.find(MagicStrings.AeroInd)
    if(AeroMarker != -1):
        return True
    else:
        return False

def isLogisticaTotalLine(lineFeed):

    LogisticaMarker = lineFeed.find(MagicStrings.LogisticaInd)
    if(LogisticaMarker != -1):
        return True
    else:
        return False

def isBeginningNewSalesPerson(lineFeed):

    BeginningMarker = lineFeed.find(MagicStrings.BeginningInd)
    if(BeginningMarker != -1):
        return True
    else:
        return False