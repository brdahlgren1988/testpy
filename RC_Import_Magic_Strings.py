__author__ = 'ReedApps'

add_Sales_Person_MySql = 'INSERT INTO salesperson' \
                         '(FirstName, LastName, SalesNumber, FinalInvoiceTotal, FinalInvoiceTaxTotal' \
                         ', FinalInvoiceDiscountTotal, FinalInvoiceLiquidTotal, FinalInvoiceFinalValueTotal' \
                         ', FinalCommissionTotal, FinalRODOLiquidValueTotal, FinalAEROLiquidValueTotal' \
                         ', FinalLogisticalLiquidValueTotal, DateCreated, DateModified' \
                         ', UserCreated, UserModified) ' \
                         'VALUES (%(firstName)s, %(lastName)s, %(salesNumber)s, %(finalInvoiceTotal)f' \
                         ', %(finalInvoiceDiscountTotal)f, %(finalInvoiceLiquidTotal)f, %(finalInvoiceFinalValueTotal)f' \
                         ', %(finalInvoiceCommissionTotal)f, finalRODOLiquidValueTotal)f, %(finalAEROLiquidValueTotal)f' \
                         ', %(finalLogisticalLiquidValueTotal)f, %(dateCreated)s, %(dateModified)s' \
                         ', %(userCreated)s, %(userModified)s)'

add_Client_MySql = 'INSERT INTO client' \
                   '(SalesPersonID, CPNJStripped, CPNJRaw, Name, ServiceProvided, BonusPercentage' \
                   ', CommissionPercentage, InvoiceTotal, InvoiceTaxTotal, InvoiceDiscountTotal' \
                   ', InvoiceLiquidTotal, InvoiceFinalValueTotal, CommissionTotal, BonusTotal' \
                   ', DateCreated, DateModified, UserCreated, UserModified) ' \
                   'VALUES (%(salesPersonID)i, %(cpnjStripped)s, %(cpnjRaw)s, %(name)s, %(serviceProvided)s' \
                   ', %(bonusPercentage)f, %(commissionPercentage)f, %(invoiceTotal)f, %(invoiceTaxTotal)f' \
                   ', %(invoiceDiscountTotal)f, %(invoiceLiquidTotal)f, %(invoiceFinalValueTotal)f' \
                   ', %(commissionTotal)f, %(bonusTotal)f, %(dateCreated)s, %(dateModified)s' \
                   ', %(userCreated)s, %(userModified)s)'

add_Invoice_MySql = 'INSERT INTO invoice' \
                    '(ClientID, SalesPersonID, InvoiceNumber, InvoiceValue, Tax' \
                    ', Discount, LiquidValue, PayDate, ServicePayDate, Difference' \
                    ', FinalValue, Percentage, BonusInvoiceValue, CommissionValue' \
                    ', DateCreated, DateModified, UserCreated, UserModified) ' \
                    'VALUES(%(clientID)i, %(salesPersonID)i, %(invoiceNumber)s, %(invoiceValue)f, %(tax)f' \
                    ', %(discount)f, %(liquidValue)f, %(payDate)s, %(servicePayDate)s, %(difference)s' \
                    ', %(finalValue)f, %(percentage)f, %(bonusInvoiceValue)f, %(commissionValue)f' \
                    ', %(dateCreated)s, %(dateModified)s, %(userCreated)s, %(userModified)s'

SQLselectWhere = 'SELECT shpr_local_cust_nbr, inv_nbr, SUM(local_net_rev_amt) AS NetRevAmt, ' \
                 '(SUM(local_net_rev_amt) + SUM(local_icms_amt)) NetRevAmt_Plus_ICMS, ' \
                 '(SUM(local_fgt_chrg_wgt_amt) + SUM(local_fgt_chrg_val_amt) + SUM(local_icms_amt) ' \
                 '+ SUM(local_tot_sphnd_addl_amt)) AS Total FROM	' \
                 'UI_ISH_L2F5_DB.br_dom_paid_shp_transp WHERE ' \
                 'UI_ISH_L2F5_DB.br_dom_paid_shp_transp.shpr_local_cust_nbr = '
SQLand = ' AND UI_ISH_L2F5_DB.br_dom_paid_shp_transp.inv_nbr IN ( '
SQLend = ' GROUP BY 1,2 ORDER BY UI_ISH_L2F5_DB.br_dom_paid_shp_transp.inv_nbr'

MySQLconn = 'DRIVER={MySQL ODBC 5.3 Unicode Driver}; SERVER=localhost; ' \
            'PORT=3306; DATABASE=rcfileimport; UID=root; PASSWORD=1qaz!QAZ;'
Teradataconn = 'DSN=EDW_L2F5_edwtestcop1'

SalesPersonInd = 'PAG.:0001'
ClientInd = '0 CLIENTE:'
InvoiceInd2015 = '/2015'
InvoiceInd2014 = '/2014'
ClientTotalInd = '0 TOTAL CLIENTE :'
SalesPersonTotalInd = '0 TOTAL ASSESSOR:'
RodoInd = 'RODOVIARIO   VALOR LIQUIDO :'
AeroInd = 'AEREO        VALOR LIQUIDO :'
LogisticaInd = 'LOGISTICA    VALOR LIQUIDO :'
BeginningInd = '0 FEDEX BRASIL LOG.TRANS.S.A-REC'