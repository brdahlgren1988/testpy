__author__ = 'ReedApps'
import pyodbc
import RC_Import_Magic_Strings as MagicStrings

def OpenMySqlConnection():
    conn = pyodbc.connect(MagicStrings.MySQLconn)
    return conn

def OpenTeradataConnection():
    conn = pyodbc.connect(MagicStrings.Teradataconn)
    return conn

def CloseConnection(conn):
    try:
        conn.close()
        print "Connection closed successfully"
    except IOError:
        print "Error closing connection"